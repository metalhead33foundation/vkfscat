TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

CONFIG(release, debug|release): LIBS += -lvulkan
CONFIG(debug, debug|release): LIBS += -lvulkan

SOURCES += main.c \
    chunk.c

HEADERS += \
    chunk.h \
    heading.h
