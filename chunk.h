#ifndef CHUNK_H
#define CHUNK_H
#include <stdint.h>
#include "heading.h"
typedef struct VmemChunk_t
{
	uint32_t size;
	uint8_t occupied;
	struct VmemChunk_t* prev;
	struct VmemChunk_t* next;
} VmemChunk;
typedef VmemChunk* pVmemChunk;

typedef void (*chunkModifier)(pVmemChunk);
typedef pVmemChunk (*chunkFindocator)(pVmemChunk); // Finder or allocator

void vmem_decoupleChunk(pVmemChunk anchor);
pVmemChunk vmem_findBeginning(pVmemChunk anchor);
pVmemChunk vmem_findEnding(pVmemChunk anchor);
uint64_t vmem_getNecessaryOffset(pVmemChunk anchor);
pVmemChunk vmem_insertBefore(pVmemChunk anchor, uint32_t size);
pVmemChunk vmem_insertAfter(pVmemChunk anchor, uint32_t size);
void vmem_destroyChunk(pVmemChunk anchor);
void vmem_destroyChunkTP(pVmemChunk anchor,chunkModifier deleter);
void vmem_destroyChunkNodes(pVmemChunk anchor);
void vmem_destroyChunkNodesTP(pVmemChunk anchor,chunkModifier deleter);

void vmem_absorbPrev(pVmemChunk anchor); // Use with caution - this does not check if prev is free or not
void vmem_absorbNext(pVmemChunk anchor); // Use with caution - this does not check if next is free or not
void vmem_absorbPrevTP(pVmemChunk anchor,chunkModifier deleter); // Use with caution - this does not check if prev is free or not
void vmem_absorbNextTP(pVmemChunk anchor,chunkModifier deleter); // Use with caution - this does not check if next is free or not

void vmem_absorbNeightbours(pVmemChunk anchor);
void vmem_absorbNeightboursTP(pVmemChunk anchor,chunkModifier deleter);

pVmemChunk vmem_createChunk(pVmemChunk anchor, uint32_t size);

#define CHUNK_FREE 0x00
#define CHUNK_OCCUPIED 0x01
#define OFFSET_ERROR (uint64_t)(int64_t)-1

#endif // CHUNK_H
