#include <stdio.h>
#include "chunk.h"

pVmemChunk chunks[4];
pVmemChunk start;

int main(int argc, char *argv[])
{
	int i = 0;
	chunks[0] = vmem_insertBefore(0,20); // I am the first one!
	chunks[1] = vmem_insertAfter(chunks[0],10); // I am supposedly second
	chunks[2] = vmem_insertAfter(chunks[1],25); // I am third
	chunks[3] = vmem_insertBefore(chunks[0],100); // I am the real first one, because I got inserted before it
	start = vmem_findBeginning(chunks[0]);
	while(start)
	{
		printf("%d - %u\n",i,start->size);
		++i;
		start = start->next;
		//destroyChunk(start->prev);
	}
	vmem_absorbNeightbours(chunks[0]);
	start = vmem_findBeginning(chunks[0]);
	i=0;
	while(start)
	{
		printf("%d - %u\n",i,start->size);
		++i;
		start = start->next;
		//destroyChunk(start->prev);
	}
	// vmem_destroyChunkNodes(chunks[0]);
	printf("Hello World!\n");
	return 0;
}
