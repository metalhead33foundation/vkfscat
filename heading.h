#ifndef HEADING_H
#define HEADING_H
#include <vulkan/vulkan.h>
#define VK_COMPOSITE_RETURNER(x,y) typedef struct x##_t \
{ \
	VkResult res; \
	y elem; \
} x

//VK_COMPOSITE_RETURNER(VmemHandle,void*);

#endif // HEADING_H
