#include "chunk.h"
#include <stdlib.h>

pVmemChunk vmem_createChunk(pVmemChunk anchor, uint32_t size)
{
	pVmemChunk head,ret;
	head = vmem_findBeginning(anchor);
	while(head)
	{
		if(head->occupied == CHUNK_FREE && head->size >= size)
		{
			if(head->size == size)
			{
				head->occupied = CHUNK_OCCUPIED;
				return head;
			}
			else
			{
				ret = vmem_insertBefore(head,size);
				head->size -= ret->size;
				return ret;
			}
		}
	}
	return 0; // Fail
}
void vmem_absorbNeightboursTP(pVmemChunk anchor,chunkModifier deleter)
{
	if(!anchor) return;
	if(anchor->occupied != CHUNK_FREE) return;
	while(anchor->prev)
	{
		if(anchor->prev->occupied == CHUNK_FREE) vmem_absorbPrevTP(anchor,deleter);
		else break;
	}
	while(anchor->next)
	{
		if(anchor->next->occupied == CHUNK_FREE) vmem_absorbNextTP(anchor,deleter);
		else break;
	}
}
void vmem_absorbNeightbours(pVmemChunk anchor)
{
	if(!anchor) return;
	if(anchor->occupied != CHUNK_FREE) return;
	while(anchor->prev)
	{
		if(anchor->prev->occupied == CHUNK_FREE) vmem_absorbPrev(anchor);
		else break;
	}
	while(anchor->next)
	{
		if(anchor->next->occupied == CHUNK_FREE) vmem_absorbNext(anchor);
		else break;
	}
}
void vmem_absorbPrevTP(pVmemChunk anchor,chunkModifier deleter)
{
	if(anchor)
	{
		if(anchor->prev)
		{
			anchor->size += anchor->prev->size;
			vmem_destroyChunkTP(anchor->prev,deleter);
		}
	}
}
void vmem_absorbNextTP(pVmemChunk anchor,chunkModifier deleter)
{
	if(anchor)
	{
		if(anchor->next)
		{
			anchor->size += anchor->next->size;
			vmem_destroyChunkTP(anchor->next,deleter);
		}
	}
}
void vmem_destroyChunkTP(pVmemChunk anchor,chunkModifier deleter)
{
	if(anchor) {
	vmem_decoupleChunk(anchor);
	(*deleter)(anchor);
	}
}
void vmem_destroyChunkNodesTP(pVmemChunk anchor,chunkModifier deleter)
{
	pVmemChunk head,temp;
	head = vmem_findBeginning(anchor);
	while(head)
	{
		temp = head;
		if(temp) head = temp->next;
		vmem_destroyChunkTP(temp,deleter);
	}
}
void vmem_absorbPrev(pVmemChunk anchor)
{
	if(anchor)
	{
		if(anchor->prev)
		{
			anchor->size += anchor->prev->size;
			vmem_destroyChunk(anchor->prev);
		}
	}
}
void vmem_absorbNext(pVmemChunk anchor)
{
	if(anchor)
	{
		if(anchor->next)
		{
			anchor->size += anchor->next->size;
			vmem_destroyChunk(anchor->next);
		}
	}
}
void vmem_destroyChunkNodes(pVmemChunk anchor)
{
	pVmemChunk head,temp;
	head = vmem_findBeginning(anchor);
	while(head)
	{
		temp = head;
		if(temp) head = temp->next;
		vmem_destroyChunk(temp);
	}
}
void vmem_destroyChunk(pVmemChunk anchor)
{
	if(anchor) {
	vmem_decoupleChunk(anchor);
	free(anchor);
	}
}
pVmemChunk vmem_insertBefore(pVmemChunk anchor, uint32_t size)
{
	pVmemChunk temp;
	temp = (pVmemChunk)malloc(sizeof(VmemChunk));
	temp->size = size;
	temp->occupied = CHUNK_FREE; // We don't designate the space as occupied yet, after all, nothing is there.
	temp->next = anchor;
	temp->prev = 0;
	if(anchor)
	{
		if(anchor->prev)
		{
			anchor->prev->next = temp;
			temp->prev = anchor->prev;
		}
		anchor->prev = temp;
	}
	return temp;
}
pVmemChunk vmem_insertAfter(pVmemChunk anchor, uint32_t size)
{
	pVmemChunk temp;
	temp = (pVmemChunk)malloc(sizeof(VmemChunk));
	temp->size = size;
	temp->occupied = CHUNK_FREE; // We don't designate the space as occupied yet, after all, nothing is there.
	temp->prev = anchor;
	temp->next = 0;
	if(anchor)
	{
		if(anchor->next)
		{
			anchor->next->prev = temp;
			temp->next = anchor->next;
		}
		anchor->next = temp;
	}
	return temp;
}

void vmem_decoupleChunk(pVmemChunk anchor)
{
	if(anchor->prev) anchor->prev->next = anchor->next;
	if(anchor->next) anchor->next->prev = anchor->prev;
}
pVmemChunk vmem_findBeginning(pVmemChunk anchor)
{
	pVmemChunk temp=anchor;
	while(temp->prev)
	{
		temp = temp->prev;
	}
	return temp;
}
pVmemChunk vmem_findEnding(pVmemChunk anchor)
{
	pVmemChunk temp=anchor;
	while(temp->next)
	{
		temp = temp->next;
	}
	return temp;
}
uint64_t vmem_getNecessaryOffset(pVmemChunk anchor)
{
	uint64_t temp = 0;
	pVmemChunk tempAnchor = vmem_findBeginning(anchor);
	if(!tempAnchor) return OFFSET_ERROR;
	while(tempAnchor->next && tempAnchor != anchor)
	{
		temp += tempAnchor->size;
		tempAnchor = tempAnchor->next;
	}
	return temp;
}
